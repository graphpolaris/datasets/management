# Management Dataset Project



## Purpose

**NOT MEANT FOR SOURCE CODE**  
The only pupose of this ```Management```project is to collect issues related to this subproject. See [link](#Usage) on how to add/modify/use

## Usage

Use the [Management Issue Board](https://git.science.uu.nl/graphpolaris/datasets/management/-/boards) to collect a backlog, in progress, etc. issues
The issues are linked with the ```GraphPolaris Teams Channel``` and will be shown there also for convenience.
